window.addEventListener('load', function Game() {
    //глобальные пирименные
    var width, height, object_size, time = 0, intervalId  = 0;
    var field = document.querySelector("#field");
    var new_game = document.querySelector("#new_game");
    var title = document.querySelector(".title"); 
    
    Request();
    new_game.addEventListener('click', function () {
        CleareRows();
        Request();
        intervalId = 0;
        clearInterval(intervalId);
    });

    //запрос к https://kde.link/test/get_field_size.php
    function Request() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'https://kde.link/test/get_field_size.php', false);
        xhr.send();
        if (xhr.status != 200) {
            alert( xhr.status + ': ' + xhr.statusText );
        } else {
            object_size= JSON.parse(xhr.response);
            width = object_size.width;
            height = object_size.height;
            CreateField (width,height);
            ArrayList(width,height);
            hoverCell();
            getWin();
        }
        
    }

    //создание игрового поля
    function CreateField (width,height){
        time = 0;
        Time();
        var rows = [];
        var cells = [];
        for (var i = 0; i < width; i++) {
            rows[i] = document.createElement('div');
            rows[i].classList.add('row');
            field.appendChild(rows[i]);
            for (var j = 0; j < height; j++) {
                cells[j] = document.createElement('div');
                cells[j].classList.add('cell');
                cells[j].index = i + ',' + j;
                rows[i].appendChild(cells[j]);
            }
        }
    }

    //очищаю поле
    function CleareRows() {
        var rows = document.querySelectorAll('.row')

        if(rows.length > 0){
            for (var i = 0; i < rows.length; i++) {
                rows[i].remove();
            }
        }
        
    }

    //заполняю поле картинками
    function ArrayList(width, height) {
        
        var ListImg = [
            "https://kde.link/test/0.png",
            "https://kde.link/test/1.png",
            "https://kde.link/test/2.png",
            "https://kde.link/test/3.png",
            "https://kde.link/test/4.png",
            "https://kde.link/test/5.png",
            "https://kde.link/test/6.png",
            "https://kde.link/test/7.png",
            "https://kde.link/test/8.png",
            "https://kde.link/test/9.png"
        ]

        var Images = [],index;
        var cells = document.querySelectorAll('.cell')

        for (var i = 0; i < (width*height)/2; i++) {
            index = Math.floor(Math.random() * (ListImg.length-1)) + 0;
            Images[i] = ListImg[index];  
        }
        
        var ImagesAll = Images.concat(Images);
        
        ImagesAll.sort(function() { 
            return Math.random() - 0.5
        });


        for (var j = 0; j < cells.length; j++) { // заполняем картинки
            cells[j].style.backgroundImage = 'url' +'(' + ImagesAll[j] + ')';
        }

    }


    function hoverCell() { // скрываем картинки
        var cells = document.querySelectorAll('.cell');
        for (var i = 0; i < cells.length; i++) {
            cells[i].classList.add('hover-cell');
        }
    }
    
    function classActive(e) { // добавляем класс к открытой картинке
        if (e.target.index && !e.target.classList.contains('active-always')) {
            e.target.classList.remove('hover-cell');
            e.target.classList.add('active');
        }
    }
    
    function classActiveAlways(arr) { // добавляем класс, если картинки совпали  
        for (var i = 0; i < arr.length; i++) {
            arr[i].classList.remove('active');
            arr[i].classList.add('active-always');
        }
    }
    
    function classHover(arr) { // если картинки не совпали
        for (var i = 0; i < arr.length; i++) {
            arr[i].classList.remove('active');
            arr[i].classList.add('hover-cell');
        }
    }
    
    function getWin() {
        var win = document.querySelectorAll('.active-always');
        var size = document.querySelectorAll('.cell');
        
        if (win.length == size.length) {
            clearInterval(intervalId);
            intervalId = 0;
            title.innerHTML = 'Вы победитель!!!!!  Ваше время '+time+' секунд!!!!';
        }else{
            title.innerHTML = 'Найди 2 одинаковые картинки';
        }
    }
    
    field.addEventListener('click', function (e) { // открываем картинки
        var active = [];
        var item1;
        var item2;
        
        classActive(e);

        console.log(e);
        
        
        active = document.querySelectorAll('.active'); // массив с открытыми картинками
        if (active.length == 2) {
        
            item1 = active[0].getAttribute('style');
            item2 = active[1].getAttribute('style');
    
            if (item1 == item2) { // проверяем, одинаковые ли картинки
                classActiveAlways(active);
            } else {
                setTimeout(classHover, 500, active);
            }

            getWin();
        }
        
        
    });

    function Time() {
        // if(intervalId == 0){
        //     clearInterval(intervalId);
        // }else{
        //     intervalId = setInterval(Time, 1000);
        //     console.log(intervalId);
            
        // }

        
        
        if(intervalId == 0){
            intervalId = setInterval(Time, 1000);
        }

        time++;
        document.querySelector('#count_time').innerHTML=time;        
    }
});